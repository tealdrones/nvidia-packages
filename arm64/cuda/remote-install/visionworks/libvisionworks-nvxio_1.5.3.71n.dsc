Format: 3.0 (quilt)
Source: libvisionworks-nvxio
Binary: libvisionworks-nvxio, libvisionworks-nvxio-dev
Architecture: arm64 all
Version: 1.5.3.71n
Maintainer: VisionWorks developers <visionworks-package@nvidia.com>
Standards-Version: 3.9.5
Build-Depends: binutils, cdbs, pkg-config, debhelper, dpkg, dpkg-dev, make, fakeroot, libdpkg-perl, file, cuda-cudart-dev-8-0 | nvidia-cuda-dev (>= 8.0), libeigen3-dev, libx11-dev, libxi-dev, libxrandr-dev, libxxf86vm-dev, libfreetype6-dev, libgstreamer1.0-dev, libgstreamer-plugins-base1.0-dev, libvisionworks-dev (= 1.5.3.71n)
Package-List:
 libvisionworks-nvxio deb libdevel extra arch=arm64
 libvisionworks-nvxio-dev deb libdevel extra arch=all
Checksums-Sha1:
 551dddcdf132c2053bb042caed28df84445b4546 300017 libvisionworks-nvxio_1.5.3.71n.orig.tar.gz
 57dfe8465c437a1bf27fb4787a19ddde32912018 16997 libvisionworks-nvxio_1.5.3.71n.debian.tar.gz
Checksums-Sha256:
 d813f7f0f2b2c1009b11b808b8efc2e8bec922d239edb2f7c78efa4e9ff429b6 300017 libvisionworks-nvxio_1.5.3.71n.orig.tar.gz
 629d3540013321d8c44b54d0e7491684f6d19d6ee3a4c689069f96609da999f1 16997 libvisionworks-nvxio_1.5.3.71n.debian.tar.gz
Files:
 1032d085aaabfa2ceb5857d446caa736 300017 libvisionworks-nvxio_1.5.3.71n.orig.tar.gz
 445063a39314f6c7f58b742593eab18a 16997 libvisionworks-nvxio_1.5.3.71n.debian.tar.gz
